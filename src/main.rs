use std::{thread, time};
use privdrop::PrivDrop;
use std::env;
use std::io::{Write, Read};
use nix::unistd::{fork, ForkResult};
use os_pipe::pipe;
use serde::{Deserialize, Serialize};
use serde_json;

#[derive(Deserialize, Serialize, Debug)]
enum Command {
    Kill,
    Text,
}

#[derive(Deserialize, Serialize)]
struct IpcTest {
    command: Command,
    message: String
}

impl IpcTest {
    pub fn new(command: Command, message: String) -> Self {
        Self { command, message }
    }

    pub fn to_json(&self) -> Vec<u8> {
        serde_json::to_string(&self).unwrap().into_bytes()
    }
}

fn main() {

    let (mut reader, writer) = pipe().unwrap();
    let (mut reader2, mut writer2) = pipe().unwrap();
    match fork().expect("fork failed") {
        ForkResult::Parent { child: _ } => {

            println!("Hello world as in parent {:?}!", env::var("USER"));
            let mut buf = [0;32];
            let len = reader.read(&mut buf).unwrap();
            println!("{} {:?}", len, buf);
            let ipc = IpcTest::new(Command::Text, "Hello World".to_owned());
            //File::create("/etc/foofile").expect("WTF could not create file in etc");
            if writer2.write(&ipc.to_json()).is_err() {
                eprintln!("Write to child failed");
            }
            thread::sleep(time::Duration::from_secs(12));

        },
        ForkResult::Child => {
            PrivDrop::default()
                .user("mikael").unwrap()
                .apply()
                .unwrap_or_else(|e| { eprintln!("Drop privileged failed? Are you not root? {}", e); });
            println!("Hello, world as {:?}!", env::var("USER"));
            let mut writer = writer.try_clone().unwrap();
            thread::sleep(time::Duration::from_secs(3));
            writer.write(b"foo").map_err(|e| { eprintln!("Write to parent failed. {}", e); });
            let mut buf = [0;128];
            let len = reader2.read(&mut buf).unwrap();
            let buf = &buf[0..len];
            let buf : IpcTest = serde_json::from_slice(&buf).expect("json parse failed");
            println!("{} {:?} {}", len, buf.command, buf.message);
            thread::sleep(time::Duration::from_secs(10));

        }
    }
}
