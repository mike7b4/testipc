# POC Ipc test

Just an hack to test if I can send messages between unprivileged and privileged user using unix pipes in Rust.

serde_json atm to (de)serialize structs for easier Ipc handling.

# Dependies

* nix
* privdrop
* os_pipe
* serde
* serde_json

# TODO

* Maybe using some binary serde instead of json for less memory usage...

